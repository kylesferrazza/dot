# DOTFILES (WIP)

My dotfiles for:
- Arch Linux
- Windows

I made the background image, [feel free to use it](https://bg.kylesferrazza.com/).

# WIP TODO
Migration from NixOS occurring. Remaining items:
- turn TODOs into issues
- title bars are showing in floating windows like `askpass`
- arandr
- clipster
- snapshot & boot menu creator
- bwmenu
- rofi fira code
- prune fonts
- try tput bel
- go through each old dotfile and remove unnecessary
  - neovim/
- go through each nixos item in ~/Docs/nix and convert back to relevant dotfiles, deleting
- finish Layout section below
- spicetify
- github action for vagrant/docker/both
- finish Layout section below
- windows
  - test WSL install and uncomment
  - powertoys
- update dotscrot, new screenshots and bg on gdrive

# Layout

```
dot/
| install.sh
|   Contains a one-liner to easily run the dot.yml playbook on localhost.
| Vagrantfile
|   Sets up an Arch Linux VM provisioned by Ansible.
| arch/
| | files/
| | scripts/
| | vars/
| | arch.svg
| | main.yml
| | makepkg.yml
| windows/
| | scripts/
| | vars/
| | main.yml
```

# Screenshots

![scrot1 here](https://drive.google.com/uc?export=download&id=1wX4P7vFZbzTqoy1EjU9oZvoa81TBrIL- "Screenshot 1")
![scrot2 here](https://drive.google.com/uc?export=download&id=1NpykkSDw95TSTUGvt0I1SYkLv54hMiQ8 "Screenshot 2")
![scrot3 here](https://drive.google.com/uc?export=download&id=1dYM2Zctt-3yTMvVNbcwXhM8C9YWLRhz6 "Screenshot 3")
