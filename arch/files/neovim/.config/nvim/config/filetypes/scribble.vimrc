au BufRead,BufNewFile *.scrbl call SetScribbleKeybinds()
function SetScribbleKeybinds()
  set syntax=scribble
  nn <Leader>rr :!./xmake render<CR><CR>
endf
