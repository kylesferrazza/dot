au BufFilePre,BufRead,BufNewFile *.[mM][dD] call SetMarkdownKeybinds()
function PandocExport(type)
  exe ":!pandoc % --filter=pandoc-citeproc -o %:r." . a:type
endf
function SetMarkdownKeybinds()
  set filetype=markdown.pandoc
  autocmd BufRead,BufNewFile *.md setlocal spell
  autocmd BufRead,BufNewFile *.md set complete+=kspell
  no <Leader>pp :call PandocExport("pdf")<CR><CR>
  no <Leader>p[ :call PandocExport("pdf")<CR>
  no <Leader>ph :call PandocExport("html")<CR><CR>
  no <Leader>pd :call PandocExport("docx")<CR><CR>
endf
