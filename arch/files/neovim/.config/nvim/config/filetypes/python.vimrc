au filetype python call SetPythonKeybinds()
function SetPythonKeybinds()
  nn <Leader>rr :!python2 %<CR>
  no <Leader>ri :T python2<CR>i
endf
