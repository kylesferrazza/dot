au BufRead,BufNewFile *.hs call SetHaskellKeybinds()
function SetHaskellKeybinds()
  nn <Leader>rr :T ghci %<CR>
endf
