au BufRead,BufNewFile *.rkt call SetRacketKeybinds()
function SetRacketKeybinds()
  nn <Leader>rt :!raco test %<CR>
  nn <Leader>rr :!racket %<CR>
  nn <Leader>; o;; <esc>79i-<esc>o<CR>;; <esc>79i-<esc>i<up>;; 
  no <Leader>ri :call RacketInteract()<CR>
endf
function RacketInteract()
  :let fname = @%
  :T racket -i
  :let cmd = "(enter! \"" . fname . "\")\r"
  :put =cmd
  :startinsert
endf
