au filetype ruby call SetRubyKeybinds()
function SetRubyKeybinds()
  nn <Leader>rr :!ruby %<CR>
  no <Leader>ri :T irb -r ./%<CR>i
endf
