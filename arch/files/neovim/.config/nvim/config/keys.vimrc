ino jk <esc>
tno jk <C-\><C-n>
cno jk <esc>
tno <C-w><C-w> <C-\><C-n><C-w><C-w>
tno <C-d> <C-\><C-n>:q<CR>
let mapleader="\<Space>"
map Y y$

no <Leader><Leader> :
no <Leader>w :w<CR>
no <Leader>q :q<CR>

no <Leader>cw g<C-g>

no <Leader>re :retab<CR>
no <Leader>hi :nohls<CR>
