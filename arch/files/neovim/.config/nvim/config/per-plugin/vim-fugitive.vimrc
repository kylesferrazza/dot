no <Leader>gd :Gdiff<CR>
no <Leader>ga :Git add %<CR>
no <Leader>gw :Gwrite<CR>
no <Leader>gr :Gread<CR>
no <Leader>gs :Gstatus<CR>
no <Leader>gc :Gcommit<CR>
no <Leader>gp :Gpush<CR>
no <Leader>gb :Gbrowse<CR>
