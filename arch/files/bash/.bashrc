[[ $- != *i* ]] && return

export EDITOR=nvim
export ALTERNATE_EDITOR=""
export TERMINAL=kitty
export WINIT_HIDPI_FACTOR=1
export WEB_BROWSER=chromium
export _JAVA_AWT_WM_NONREPARENTING=1
export TERM=xterm-256color
export DIFFPROG=meld
export LIBVA_DRIVER_NAME=iHD
export JAVA_HOME=/usr/lib/jvm/default
export QT_QPA_PLATFORMTHEME=gtk2

[ -x /bin/fish ] && SHELL=/bin/fish exec /bin/fish -l
