set -l base03  "--bold black"
set -l base02  "black"
set -l base01  "--bold green"
set -l base00  "--bold yellow"
set -l base0   "--bold blue"
set -l base1   "--bold cyan"
set -l base2   "white"
set -l base3   "--bold white"
set -l yellow  "yellow"
set -l orange  "--bold red"
set -l red     "red"
set -l magenta "magenta"
set -l violet  "--bold magenta"
set -l blue    "blue"
set -l cyan    "cyan"
set -l green   "green"

set -g fish_color_normal       $base0
set -g fish_color_command      $base0
set -g fish_color_quote        $cyan
set -g fish_color_redirection  $base0
set -g fish_color_end          $base0
set -g fish_color_error        $red
set -g fish_color_param        $blue
set -g fish_color_comment      $base01
set -g fish_color_match        $cyan
set -g fish_color_search_match "--background=$base02"
set -g fish_color_operator     $orange
set -g fish_color_escape       $cyan

set -gx PATH ~/.bin ~/.local/bin ~/.rbenv/shims ~/.npm-global/bin ~/.cargo/bin $PATH

set fish_greeting
fish_vi_key_bindings
bind -M insert -m default jk backward-char force-repaint

alias pb ix
alias vim nvim
alias updsrcinfo "makepkg --printsrcinfo > .SRCINFO"

if status --is-login; and test (tty) = "/dev/tty1"
  exec startx
end

eval (starship init fish)
