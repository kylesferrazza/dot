function listento
  if test -n "$argv" -a -e "$argv"
    set a $argv
  else
    set a (which $argv)
  end
  cat $a | padsp tee /dev/audio > /dev/null
end
