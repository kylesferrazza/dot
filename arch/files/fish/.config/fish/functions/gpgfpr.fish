function gpgfpr
  gpg --fingerprint  $argv | head -n 2 | tail -n 1 | sed 's/ //g'
end
