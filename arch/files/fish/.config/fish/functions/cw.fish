function cw 
  if test -z "$argv"
    echo "Need arguments for catwhich!"
    return
  end
  if test -e "$argv"
    cat $argv
  else
    if which $argv > /dev/null
      cat (which $argv)
    end
  end
end
