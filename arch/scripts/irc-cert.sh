#!/bin/bash

# Generating IRC certificate...
mkdir -pv ~/.irssi
cd ~/.irssi
echo -e "\n\n\n\n\n\n\n" | openssl req -newkey rsa:2048 -days 730 -x509 -keyout irssi.key -out irssi.crt -nodes
cat irssi.crt irssi.key > irssi.pem
chmod 600 irssi.pem
rm -vf irssi.crt irssi.key

# Setting up IRSSI scripts...
mkdir -pv scripts/autorun
cd scripts
wget https://raw.githubusercontent.com/lifeforms/irssi-smartfilter/master/smartfilter.pl -O smartfilter.pl
cd autorun
ln -sf ../smartfilter.pl smartfilter.pl
