#!/bin/bash

curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
nvim -c :PlugInstall -c q -c q
nvim -c :UpdateRemotePlugins -c q -c q
fish -c "tic $HOME/xterm-256color.ti"

pushd ~/.config/nvim/plugged/YouCompleteMe/
python3 install.py --ts-completer --rust-completer --java-completer
popd
