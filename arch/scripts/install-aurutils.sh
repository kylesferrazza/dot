#!/bin/bash
set -euo pipefail

BLDROOT=$(su kyle -c 'mktemp -d')
pushd $BLDROOT

su kyle -c bash <<EOF
  set -euo pipefail
  gpg --keyserver keys.gnupg.net --recv-key 6BC26A17B9B7018A
  git clone https://aur.archlinux.org/aurutils.git $BLDROOT
EOF

(
  source PKGBUILD
  pacman -Syu --noconfirm --needed --asdeps "${makedepends[@]}" "${depends[@]}"
)

su kyle -c bash <<EOF
  makepkg -sr
EOF

pacman -U --noconfirm aurutils-2.3.3-1-any.pkg.tar

popd
rm -rf "$BLDROOT"
