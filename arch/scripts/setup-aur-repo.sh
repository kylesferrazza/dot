#!/bin/bash
set -euo pipefail

cat > /etc/pacman.d/aur <<EOF
[options]
CacheDir = /var/cache/pacman/pkg
CacheDir = /var/cache/pacman/aur
CleanMethod = KeepCurrent

[aur]
SigLevel = Optional TrustAll
Server = file:///var/cache/pacman/aur
EOF

INCL="Include = /etc/pacman.d/aur"

if ! grep "$INCL" /etc/pacman.conf; then
  echo "$INCL" >> /etc/pacman.conf
fi

install -d /var/cache/pacman/aur -o kyle

su kyle -c "repo-add /var/cache/pacman/aur/aur.db.tar"

pacman -Syu --noconfirm
